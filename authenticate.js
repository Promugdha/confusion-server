var passport = require('passport');
var LocalPassstrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');
var facebookTokStrategy = require('passport-facebook-token');
const User = require('./models/users');
const config = require('./config');
//const dotenv = require('dotenv').config();

passport.use(new LocalPassstrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

exports.getToken = (user) => {
    return jwt.sign(user, config.SECRET,
        { expiresIn: 3600 });
};

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.SECRET;

exports.jwtPassport = passport.use(new JwtStrategy(opts,
    (jwt_payload, done) => {
        console.log("JWT payload: ", jwt_payload);
        User.findOne({ _id: jwt_payload._id }, (err, user) => {
            if (err) {
                return done(err, false);
            }
            else if (user) {
                console.log(user);
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        });
    }));


exports.verifyuser = passport.authenticate('jwt', { session: false });


exports.adminuser = function (req, res, next) {
    console.log(req.user.admin)
    if (req.user.admin !== 'true') {
        console.log("if");
        var err = new Error("You are not authenticated to do this operation");
        err.statusCode = 403;
        return next(err);
    }
    else if (req.user.admin === 'true') {
        console.log("else")
        return next();
    }
}

var mat = {}
mat.clientID = config.facebook.clientID
mat.clientSecret = config.facebook.clientSecret

exports.facebookauth = passport.use(new facebookTokStrategy(mat,
    (accessToken, refreshToken, profile, done) => {
        User.findOne({ facebookId: profile.id }, (err, user) => {
            if (err) {
                return done(err, false)
            }
            if (!err && user !== null) {
                return done(null, user)
            }
            else {
                user = new User({ username: profile.displayName });
                user.facebookId = profile.id;
                user.firstname = profile.name.givenName;
                user.lastname = profile.name.familyName;
                user.save((err, user) => {
                    if (err)
                        return done(err, false);
                    else
                        return done(null, user);
                })
            }
        })
    }
))

