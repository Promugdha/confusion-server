const mongoose = require('mongoose');
const schema = mongoose.Schema;

const commentSchema = new schema({
    rating: {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment: {
        type: String,
        required: true,
    },
    author: {
        type: schema.Types.ObjectId,
        ref: 'user'
    },
    dish: {
        type: schema.Types.ObjectId,
        ref: 'dish'
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('comment', commentSchema);