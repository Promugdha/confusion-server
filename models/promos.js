const mongoose = require('mongoose');
const schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const promSchema = new schema({
    name: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    label: {
        type: String,
        default: " "
    },
    featured: {
        type: Boolean,
        default: false
    },
    price: {
        type: Currency,
        required: true,
        min: 0
    }
}, {
    timestamps: true
});

var dishes = mongoose.model('promo', promSchema);
module.exports = dishes;