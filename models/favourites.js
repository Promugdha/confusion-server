const mongoose = require('mongoose');
const schema = mongoose.Schema;

var favSchema = new schema({
    users: {
        type: schema.Types.ObjectId,
        ref: 'user'
    },
    dishes: [{
        type: schema.Types.ObjectId,
        ref: 'dish'
    }]
}, {
    timestamps: true
})

module.exports = mongoose.model('favourite', favSchema);