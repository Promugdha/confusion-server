var mongoose = require('mongoose');
var schema = mongoose.Schema;
var passlocmon = require('passport-local-mongoose');

var userSchema = new schema({
    firstname: {
        type: String,
        default: ' '
    },
    lastname: {
        type: String,
        default: ' '
    },
    facebookId: String,
    admin: {
        type: String,
        default: false
    }
});

userSchema.plugin(passlocmon);

module.exports = mongoose.model('user', userSchema);