const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const Leads = require('../models/leaders');
const authenticate = require('../authenticate');
const cors = require('./cors');

const leaderRoute = express.Router();

leaderRoute.use(bodyparser.json());

leaderRoute.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Leads.find(req.query)
            .then((leads) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(leads);
            }, err => next(err))
            .catch(err => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Leads.create(req.body)
            .then(lead => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(lead);
            }, err => next(err))
            .catch(err => next(err));
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end(req.method + " not possible");
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Leads.remove({})
            .then(leads => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(leads);
            }, err => next(err))
            .catch(err => next(err));
    })
leaderRoute.route('/:leadid')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Leads.findById(req.params.leadid)
            .then(lead => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(lead);
            }, err => next(err))
            .catch(err => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403
        res.end(req.method + " not possible");
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Leads.findByIdAndUpdate(req.params.leadid, { $set: req.body }, { new: true })
            .then(lead => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(lead);
            }, err => next(err))
            .catch(err => next(err))
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Leads.findByIdAndRemove(req.params.leadid)
            .then(lead => {
                res.statusCode = 200;
                res.setHeader('Content-type', 'application/json');
                res.json(lead);
            }, err => next(err))
            .catch(err => next(err))
    })

module.exports = leaderRoute;    