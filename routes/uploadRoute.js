const express = require('express');
const bodyparser = require('body-parser');
const authenticate = require('../authenticate');
const multer = require('multer');
const cors = require('./cors');

const storage = multer.diskStorage({

    destination: (req, file, cb) => {
        cb(null, 'public/images');
    },

    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }

});

const filefilters = (req, file, cb) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return cb(new Error('you cannot upload file with this .ext'), false);
    }
    cb(null, true)
}

const upload = multer({ storage: storage, fileFilter: filefilters })

const uploadRouter = express.Router();

uploadRouter.use(bodyparser.json())

uploadRouter.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end('GET operation not supported on /imgUp');
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, upload.single('imageFile'), (req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(req.file)
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /imgUp');
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end('DELETE operation not supported on /imgUp');
    });

module.exports = uploadRouter;
