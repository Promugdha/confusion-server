const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const Promos = require('../models/promos');
const authenticate = require('../authenticate');
const cors = require('./cors');

const promoRoute = express.Router();

promoRoute.use(bodyparser.json());

promoRoute.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Promos.find(req.query)
            .then((proms) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(proms);
            }, err => next(err))
            .catch(err => next(err));
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Promos.create(req.body)
            .then((prom) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(prom);
            }, err => next(err))
            .catch(err => next(err));
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end(req.method + " not possible");
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Promos.remove({})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'appliaction/json');
                res.json(resp)
            }, err => next(err))
            .catch(err => next(err))
    })
promoRoute.route('/:promoid')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Promos.findById(req.params.promoid)
            .then((prom) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'appliaction/json');
                res.json(prom);
            }, err => next(err))
            .catch(err => (err));
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403
        res.end(req.method + " not possible");
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Promos.findByIdAndUpdate(req.params.promoid, {
            $set: req.body
        }, { new: true })
            .then((prom) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(prom);
            }, err => next(err))
            .catch(err => next(err));
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Promos.findByIdAndRemove(req.params.promoid)
            .then(dish => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dish);
            }, err => next(err))
            .catch(err => next(err));
    })

module.exports = promoRoute;    