var express = require('express');
var bodyparser = require('body-parser');
var passport = require('passport');

var User = require('../models/users');
var authenticate = require('../authenticate');
var cors = require('./cors');

var router = express.Router();
router.use(bodyparser.json());
/* GET users listing. */
router.options('*', cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
router.get('/', cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
  User.find({})
    .then(dish => {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'appliaction/json');
      res.json(dish);
    }, err => next(err))
    .catch(err => next(err))
});

router.post('/signup', cors.corsWithOptions, (req, res, next) => {
  User.register(new User({ username: req.body.username }), req.body.password, (err, user) => {
    if (err) {
      res.statusCode = 500;
      res.header('Content-type', 'application/json');
      res.json({ err: err })
    }
    else {
      if (req.body.firstname) {
        user.firstname = req.body.firstname;
      }
      if (req.body.lastname) {
        user.lastname = req.body.lastname;
      }
      user.save((err, user) => {
        if (err) {
          res.statusCode = 500;
          res.header('Content-type', 'application/json');
          res.json({ err: err })
        }
        passport.authenticate('local')(req, res, () => {
          res.statusCode = 200;
          res.header('Content-type', 'application/json');
          res.json({ success: true, status: "registered succesfully" })
        });
      })
    }
  });
});

router.post('/login', cors.corsWithOptions, (req, res) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      return next(err)
    }
    if (!user) {
      res.statusCode = 401;
      res.header('Content-type', 'application/json');
      res.json({ success: false, err: info, status: "log in unsuccesful" })
    }
    req.logIn(user, (err) => {
      if (err) {
        res.statusCode = 401;
        res.header('Content-type', 'application/json');
        res.json({ success: false, err: "could not log in", status: "log in unsuccesful" })
      }
      var token = authenticate.getToken({ _id: req.user._id });
      res.statusCode = 200;
      res.header('Content-type', 'application/json');
      res.json({ success: true, token: token, status: "logged in succesfully" })
    })
  })(req, res)
});

router.get('/facebook/token', passport.authenticate('facebook-token'), (req, res) => {
  if (req.user) {
    var token = authenticate.getToken({ _id: req.user._id });
    res.statusCode = 200;
    res.header('Content-type', 'application/json');
    res.json({ success: true, token: token, status: "logged in succesfully" })
  }
})

router.get('/logout', cors.corsWithOptions, (req, res) => {
  if (req.session) {
    req.session.destroy();
    res.clearCookie('my-sess');
    res.redirect('/');
  }
  else {
    var err = new Error('You are not logged in!');
    err.status = 403;
    next(err);
  }
});

router.get('/checkJWTtoken', cors.corsWithOptions, (req, res) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err)
      return next(err);

    if (!user) {
      res.statusCode = 401;
      res.setHeader('Content-Type', 'application/json');
      return res.json({ status: 'JWT invalid!', success: false, err: info });
    }
    else {
      res.statusCode = 200;
      res.setHeader('Content-Type', 'application/json');
      return res.json({ status: 'JWT valid!', success: true, user: user });

    }
  })(req, res);
});

module.exports = router;
