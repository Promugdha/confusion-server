const express = require('express');
const bodyparser = require('body-parser');
const Comments = require('../models/comments');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const commentRoute = express.Router();

commentRoute.use(bodyparser.json());

commentRoute.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Comments.findById(req.query)
            .populate('author')
            .then((com) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(com);
            }, (err) => { next(err) })
            .catch((err) => next(err))
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        if (req.body !== null) {
            req.body.author = req.user._id
            Comments.create(req.body)
                .then(com => {
                    Comments.findById(com._id)
                        .populate('author')
                        .then(com => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(com);
                        })
                }, err => next(err))
                .catch(err => next(err))
        }
        else {
            err = new Error('Comments does not exist');
            err.status = 404;
            return next(err);
        }
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Comments.remove({})
            .then(resp => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            })
    });
commentRoute.route('/:commentid')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Comments.findById(req.params.commentid)
            .populate('author')
            .then((com) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(com);
            }, (err => next(err)))
            .catch(err => next(err))
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        Comments.findById(req.params.commentid)
            .then((com) => {
                if (com != null) {
                    if (com.author.equals(req.user._id)) {
                        // console.log(req.user._id)
                        req.body.author = req.user._id
                        Comments.findByIdAndUpdate(req.params.commentid, {
                            $set: req.body
                        }, { new: true })
                            .then(com => {
                                Comments.findById(com._id)
                                    .populate('author')
                                    .then(resp => {
                                        res.statusCode = 200;
                                        res.setHeader('Content-Type', 'application/json');
                                        res.json(resp);
                                    })
                            }, (err => next(err)))
                    }
                    else {
                        err = new Error("you are not the owner");
                        err.status = 404;
                        return next(err)
                    }
                }
                else if (com == null) {
                    err = new Error(" comment does not exist");
                    err.status = 404;
                    return next(err)
                }
                else {
                    err = new Error("comment  does not exist");
                    err.status = 404;
                    return next(err)
                }

            }, (err => next(err)))
            .catch(err => next(err))
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        Comments.findById(req.params.dishid)
            .then((com) => {
                if (com != null) {
                    if (!com.author.equals(req.user._id)) {
                        err = new Error("you are not the owner");
                        err.status = 404;
                        return next(err)
                    }
                    Comments.findByIdAndRemove(req.params.commentid)
                        .then((resp) => {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(resp);
                        }, (err => next(err)))
                        .catch((err) => next(err))
                }
                else {
                    err = new Error('comment ' + req.params.commentid + " does not exist");
                    err.status = 404;
                    return next(err)
                }
            }, ((err) => next(err)))
            .catch((err) => next(err))
    });
module.exports = commentRoute;