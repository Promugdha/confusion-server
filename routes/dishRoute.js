const express = require('express');
const bodyparser = require('body-parser');
const Dishes = require('../models/dishes');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const dishRoute = express.Router();

dishRoute.use(bodyparser.json());

dishRoute.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Dishes.find(req.query)
            .populate('comments.author')
            .then((dishes) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dishes);
            }, (err) => { next(err) })
            .catch((err) => next(err))
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Dishes.create(req.body)
            .then((dish) => {
                console.log("updated the db with ", dish);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dish);
            }, ((err) => { next(err) }))
            .catch((err) => next(err))
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Dishes.remove({})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp)
            }, ((err) => next(err)))
            .catch((err) => next(err))
    });
dishRoute.route('/:dishid')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Dishes.findById(req.params.dishid)
            .populate('comments.author')
            .then((dish) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dish);
            }, (err => next(err)))
            .catch(err => next(err))
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Dishes.findByIdAndUpdate(req.params.dishid, {
            $set: req.body
        }, { new: true })
            .then((dish) => {
                console.log("updated prev dish with ", dish);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dish);
            }, (err => next(err)))
            .catch(err => next(err))
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, authenticate.adminuser, (req, res, next) => {
        Dishes.findByIdAndRemove(req.params.dishid)
            .then((resp) => {
                console.log("deleted the particular ", dish);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp)
            }, ((err) => next(err)))
            .catch((err) => next(err))
    });

module.exports = dishRoute;