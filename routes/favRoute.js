const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const authenticate = require('../authenticate');
const Favourites = require('../models/favourites');

const favs = express.Router();

favs.use(bodyparser.json());

favs.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyuser, (req, res, next) => {
        Favourites.find({})
            .populate('users')
            .populate('dishes')
            .then((dish) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json')
                res.json(dish[0])
            }, (err) => next(err))
            .catch(err => next(err))
    })
    .post(cors.cors, authenticate.verifyuser, (req, res, next) => {
        Favourites.findOne({ users: req.user._id })
            .then(fav => {
                for (const i in req.body) {
                    //console.log(req.body)
                    var ind = fav.dishes.indexOf(req.body[i]._id.toString())
                    //console.log(ind)
                    if (ind < 0) {
                        fav.dishes.push(req.body[i])
                    }
                }
                /*fav.save((err, fav) => {
                    if (err) {
                        throw err;
                        console.log("err")
                    }
                    else {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json')
                        res.json(fav)
                    }
                })*/
                fav.save()
                    .then(fav => {
                        Favourites.findById(fav._id)
                            .populate('users')
                            .populate('dishes')
                            .then(fav => {
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json')
                                res.json(fav)
                            })
                    })
            })
            .catch(err => next(err))
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        Favourites.remove({})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp)
            }, ((err) => next(err)))
            .catch((err) => next(err))
    });
favs.route('/:dishId')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, authenticate.verifyuser, (req, res, next) => {
        Favourites.findOne({ users: req.user._id })
            .then(favs => {
                if (!favs) {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json")
                    return res.json({ "exists": false, "favs": favs })
                }
                else {
                    if (favs.dishes.indexOf(req.params.dishId) < 0) {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json")
                        return res.json({ "exists": false, "favs": favs })

                    }
                    else {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json")
                        return res.json({ "exists": true, "favs": favs })
                    }
                }
            }, err => next(err))
            .catch(err => next(err))
    })
    .post(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        Favourites.findOne({ users: req.user._id })
            .then(fav => {
                if (fav == null) {
                    Favourites.create(req.body)
                        .then(fav => {
                            Favourites.findById(fav._id)
                                .then(fav => {
                                    fav.users = req.user._id
                                    fav.dishes.push(req.params.dishId)
                                    /*fav.save((err, fav) => {
                                        if (err) {
                                            throw err;
                                        }
                                        else {
                                            res.statusCode = 200;
                                            res.setHeader('Content-Type', 'application/json')
                                            res.json(fav)
                                        }
                                    })*/
                                    fav.save()
                                        .then(fav => {
                                            Favourites.findById(fav._id)
                                                .populate('users')
                                                .populate('dishes')
                                                .then(fav => {
                                                    res.statusCode = 200;
                                                    res.setHeader('Content-Type', 'application/json')
                                                    res.json(fav)
                                                })
                                        })
                                })
                        })
                }
                else {
                    var rep = fav.dishes.filter(fav => fav.toString() === req.params.dishId.toString())[0];
                    //console.log("ht", rep, "he")
                    if (!rep) {
                        fav.dishes.push(req.params.dishId)
                    }
                    /* fav.save((err, fav) => {
                         if (err) {
                             throw err;
                             console.log("err")
                         }
                         else {
                             res.statusCode = 200;
                             res.setHeader('Content-Type', 'application/json')
                             res.json(fav)
                         }
                     })*/
                    fav.save()
                        .then(fav => {
                            Favourites.findById(fav._id)
                                .populate('users')
                                .populate('dishes')
                                .then(fav => {
                                    res.statusCode = 200;
                                    res.setHeader('Content-Type', 'application/json')
                                    res.json(fav)
                                })
                        })
                }
            })
    })
    .put(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        res.statusCode = 403;
        res.end("operation not possible");
    })
    .delete(cors.corsWithOptions, authenticate.verifyuser, (req, res, next) => {
        Favourites.findOne({ users: req.user._id })
            .then(fav => {

                var index = fav.dishes.indexOf(req.params.dishId)
                if (index >= 0) {
                    /*for (var i = fav.dishes.length - 1; i >= 0; i--) {
                        if(fav.dishes[i]===null){
                            fav.dishes
                        }
                    }*/
                    fav.dishes.splice(index, 1)
                    fav.save((err, fav) => {
                        if (err) {
                            throw err;
                            console.log("err")
                        }
                        else {
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json')
                            res.json(fav)
                        }
                    })
                }
            })
    })
module.exports = favs;
